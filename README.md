# mp-rfid

mp-rfid a set of tools design to create a cloud based RFID system.

Server side is running on Google's AppEngine, client side is a Raspberry Pi.

Originally designed and created for http://www.makersplace.org.au/.


### How do I get set up?

Server side:

	* create an AppEngine project, like example.appspot.com
	* upload all code to AppEngine

Client side:

	* install a Raspberry Pi with raspbian
	* install PiFace
	* install a USB RFID reader
	* download and run example.appspot.com/c/install.sh


### Licence 

The licence applies to all files in this repo unless:

	* otherwise stated, such as in the bootstrap directory
	* not my product, such as favicon.ico


The MIT License (MIT)

Copyright (c) 2015 Miklos Honti

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
