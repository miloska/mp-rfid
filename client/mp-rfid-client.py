#!/usr/bin/env python 
#
# RDIF access control for makersplace
#
import httplib
import HTMLParser
import logging
import sqlite3
import time
import threading
import requests
import json
import RPi.GPIO as GPIO

DEBUG=1
_HTTP_HOST = 'mp-rfid.appspot.com'
_HTTP_TIMEOUT = 2
_SQLITE_DB_PATH = ''
_SQLITE_DB_NAME = 'makerspace-201604.db'
_OFFLINE_SYNC_DELAY = 300
_OFFLINE_CACHE_TIMEOUT = 0
_POWER_PIN = 4
_LED_PIN = 16
# _OFFLINE_CACHE_TIMEOUT = 24*60*60

def getserial():
  # Extract serial from cpuinfo file
  cpuserial = '0000000000000000'
  try:
    f = open('/proc/cpuinfo', 'r')
    for line in f:
      if line[0:6] == 'Serial':
          cpuserial = line[10:26]
    f.close()
  except:
    cpuserial = 'ERROR000000000'
  logger(info='getserial() - cpuserial: %s' % cpuserial)
  return cpuserial

def check_card(uuid, cardnumber):
  # sanity check - could be more here, need more real life examples what to filter for
  if len(cardnumber) < 4 or len(cardnumber) > 30:
    info = 'NOK Invalid/unexpected card ID: %s' % cardnumber
    logger(cardnumber=cardnumber, result=0, info=info)
    return 0
  offline_seconds = card_check_offline(cardnumber=cardnumber)
  if offline_seconds > 0:
    info = 'OK offline card ID: %s, seconds: %s' % (cardnumber, offline_seconds)
    logger(cardnumber=cardnumber, result=1, info=info)
    return offline_seconds
  elif offline_seconds == 0:
    online_seconds = card_check_online(uuid=uuid, cardnumber=cardnumber, timeout=_HTTP_TIMEOUT)
    if online_seconds >= 0:
      info = 'OK online card ID: %s, seconds: %s' % (cardnumber, online_seconds)
      logger(cardnumber=cardnumber, result=1, info=info)
      return online_seconds
  else:
    info = 'NOK catchall card ID: %s' % cardnumber
    logger(cardnumber=cardnumber, result=0, info=info)
    return 0

def card_check_online(uuid, cardnumber, timeout):
  try:
    logger(info='card_check_online() - start')
    url = 'https://'+ _HTTP_HOST + '/auth/%s/%s' % (uuid, cardnumber)
    https_resp = requests.get(url=url, verify=True)
    logger(info='card_check_online() - response status: %s, body: %s' % (https_resp, https_resp.text))
    data = json.loads(https_resp.text)
    logger(info='card_check_online() - end, data: %s' % data)
    if int(data):
      return int(data)
    else:
      return 0
  except Exception as e:
    print (str(e))
    return 0

def card_check_offline(cardnumber):
  logger(info='Checking card offline for cardnumber: %s' % cardnumber)
  sql = ('SELECT timestamp FROM last_sucsessful_dump LIMIT 1;')
  logger(info=sql)
  result = global_sqlite_con.execute(sql).fetchone()
  ts = int(time.time())
  if _OFFLINE_CACHE_TIMEOUT and result[0] + _OFFLINE_CACHE_TIMEOUT < ts:
    return 0
  sql = ('SELECT authtime FROM valid_cards WHERE cardnumber="%s"' % cardnumber)
  logger(info=sql)
  result = global_sqlite_con.execute(sql).fetchone()
  try:
    if result[0] > 0:
      return result[0]
  except: 
    return 0
  return 0
    
def take_action(uuid, cardnumber, seconds):
  logger(info='Opening door for card #%s.' % cardnumber)
  try:
    s = int(seconds)
  except:
    return
  if s > 0:
    relay.power_on()
    print 'Door open'
    time.sleep(s)
    relay.power_off()
    
def logger(cardnumber='', result='', info=''):
  cardnumber=str(cardnumber)
  result=str(result)
  info=str(info)
  if DEBUG:
    print '|'.join([uuid,cardnumber,result,info])
  logging.info('|'.join([uuid,cardnumber,result,info]))
  write_sqllog(uuid=uuid, cardnumber=cardnumber, result=result, info=info)
    
def write_sqllog(uuid, cardnumber='', result='', info=''):
  try:
    ts = int(time.time())
    sql = ('INSERT INTO log (timestamp, uuid, cardnumber, result, info) '
           'VALUES ("%d","%s","%s","%s","%s"); ' % (ts, uuid, cardnumber, result, info))
    global_sqlite_con.execute(sql)
    global_sqlite_con.commit()
  except:
    logging.info('SQL logging failed, keep going')

def sqlite_setup():
  global global_sqlite_con
  global_sqlite_con = sqlite3.connect(_SQLITE_DB_PATH + _SQLITE_DB_NAME)
  global_sqlite_con.execute(
      'CREATE TABLE IF NOT EXISTS '
      'log (timestamp INT, uuid TEXT, cardnumber TEXT, result TEXT, info TEXT)')
  global_sqlite_con.execute(
      'CREATE TABLE IF NOT EXISTS '
      'valid_cards (cardnumber TEXT PRIMARY KEY, authtime INT, valid INT)')
  global_sqlite_con.execute(
      'CREATE TABLE IF NOT EXISTS '
      'last_sucsessful_dump (timestamp INT)')
  sql = 'INSERT OR REPLACE INTO last_sucsessful_dump (timestamp) VALUES (0);'
  global_sqlite_con.execute(sql)
  global_sqlite_con.commit()

def update_offline_database(uuid):
  sqlite_con = sqlite3.connect(_SQLITE_DB_PATH + _SQLITE_DB_NAME)
  while 1:
    try:
      logger(info='update_offline_database() - start')
      url = 'https://' + _HTTP_HOST + '/dump/' + uuid
      https_resp = requests.get(url=url, verify=True)
      logger(info='update_offline_database() - response status: %s' % https_resp)
      responses = json.loads(https_resp.text)
      logger(info='update_offline_database() - dump: %s' % responses)
      sql = 'UPDATE valid_cards SET valid=0;'
      sqlite_con.execute(sql)
      for response in responses:
        sql = ('INSERT OR REPLACE INTO valid_cards (cardnumber, authtime) VALUES ("%s", %d); ' %
               (response['cardnumber'], response['authtime']))
        sqlite_con.execute(sql)
        sql = ('UPDATE valid_cards SET valid = 1 WHERE cardnumber = "%s";' % response['cardnumber'])
        sqlite_con.execute(sql)
      sql = 'DELETE FROM valid_cards WHERE valid <> 1;'    
      sqlite_con.execute(sql)

      sql = 'DELETE FROM last_sucsessful_dump WHERE 1;'
      sqlite_con.execute(sql)
      ts = int(time.time())
      sql = 'INSERT OR REPLACE INTO last_sucsessful_dump (timestamp) VALUES (%d);' % ts
      sqlite_con.execute(sql)
      sqlite_con.commit()
      logger(info='update_offline_database() - sleep')
    except Exception as e:
      print str(e)
      for i in range(10):
        relay.led_off()
        time.sleep(0.5)
        relay.led_on()
    time.sleep(_OFFLINE_SYNC_DELAY)

class Relay():
  "Abstrct relay - possible multiple hardware support in the future."
  def __init__(self, power_pin=0, led_pin=0):
    self.power_pin = power_pin
    self.led_pin = led_pin
    GPIO.setmode(GPIO.BCM)
    if self.power_pin:
      GPIO.setup(self.power_pin, GPIO.OUT)
    if self.led_pin:
      GPIO.setup(self.led_pin, GPIO.OUT)
  def power_on(self):
    if self.power_pin:
      GPIO.output(self.power_pin, GPIO.HIGH)
  def power_off(self):
    if self.power_pin:
      GPIO.output(self.power_pin, GPIO.LOW)
  def led_on(self):
    if self.led_pin:
      GPIO.output(self.led_pin, GPIO.HIGH)
  def led_off(self):
    if self.led_pin:
      GPIO.output(self.led_pin, GPIO.LOW)
      
  
def main():
  # ID to identify the device. Initially I'll use RPi's serial, but could be other ID
  global uuid
  global relay
  uuid = getserial()
  sqlite_setup()
  t = threading.Thread(target=update_offline_database, args=(uuid,))
  t.daemon = True
  t.start()
  relay.led_on()

  while 1:
    global_sqlite_con.commit()
    cardnumber = raw_input('Card #:')
    logger(info='Checking card #: %s' % cardnumber)
    print cardnumber
    if cardnumber == '123':
      logger(info='magic number received, exiting')
      relay.led_off()
      quit()
    seconds = check_card(uuid=uuid, cardnumber=cardnumber)
    if seconds > 0:
      take_action(uuid=uuid, cardnumber=cardnumber, seconds=seconds)

if __name__ == "__main__":
  global_sqlite_con = None
  uuid = ''
  relay = Relay(power_pin=_POWER_PIN, led_pin=_LED_PIN)
  main()
