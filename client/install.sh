# install.sh for mp-rfid-client.py
# designed to run on stock install raspbian
# 
set -e
set -x

# Stolen from raspi-config
get_init_sys() {
  if command -v systemctl > /dev/null && systemctl | grep -q '\-\.mount'; then
    SYSTEMD=1
  elif [ -f /etc/init.d/cron ] && [ ! -h /etc/init.d/cron ]; then
    SYSTEMD=0
  else
    echo "Unrecognised init system"
    return 1
  fi
}

if [ "$(id -u)" -ne 0 ]; then
  /bin/echo "Please run as root or with sudo"
  exit
fi

/usr/bin/apt-get install python-requests python-rpi.gpio

# Download script.
if [ -f /usr/local/bin/mp-rfid-client.py ]; then
  /bin/cp /usr/local/bin/mp-rfid-client.py /usr/local/bin/mp-rfid-client.py-`date +%Y-%m-%d--%H-%M-%S`
fi

/usr/bin/wget -q -O /usr/local/bin/mp-rfid-client.py https://mp-rfid.appspot.com/c/mp-rfid-client.py

/bin/chmod +x /usr/local/bin/mp-rfid-client.py

# Set up local environment
# if [ ! -d /var/local/mp-rfid-client ]; then
#   /bin/mkdir /var/local/mp-rfid-client
# fi

if ! /usr/bin/id mp; then
  /usr/sbin/adduser --shell=/usr/local/bin/mp-rfid-client.py --disabled-password --gecos "mp-rfid" mp
  /usr/sbin/usermod -G mp,spi,gpio mp
fi

# Set up auto login
get_init_sys
if [ $SYSTEMD -eq 1 ]; then
    sed -i 's/autologin pi/autologin mp/' /etc/systemd/system/autologin@.service
    systemctl set-default multi-user.target
    ln -fs /etc/systemd/system/autologin@.service /etc/systemd/system/getty.target.wants/getty@tty1.service
  else
    /bin/cp /etc/rc.local /etc/rc.local-`date +%Y-%m-%d--%H-%M-%S`

    if ! /bin/grep -q chvt /etc/rc.local; then
      /usr/bin/head -n -1 /etc/rc.local > /tmp/rc.local
      /bin/echo "/bin/chvt 2" >> /tmp/rc.local
      /bin/echo "exit 0" >> /tmp/rc.local
      /bin/cat /tmp/rc.local > /etc/rc.local
      /bin/rm -f /tmp/rc.local
    else
      /bin/echo "/etc/rc.local configured"
    fi

    if ! /bin/grep -q mp-rfid-client /etc/inittab; then
      /bin/cp /etc/inittab /etc/inittab-`date +%Y-%m-%d--%H-%M-%S`
      /bin/sed -i 's/2:23.*/2:23:respawn:\/bin\/login \-f mp tty2 <\/dev\/tty2 >\/dev\/tty2 2>\&1/' /etc/inittab
    else
      /bin/echo "/etc/inittab configured"
    fi
fi
/bin/echo -e "Installation done!\n"
