import datetime
import logging
import jinja2
import json
import time
import os
import webapp2

from google.appengine.api import users
from google.appengine.ext import ndb


def data_key(key='default_key'):
  return ndb.Key('key', key)

jinja_environment = jinja2.Environment(
  loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
  extensions=['jinja2.ext.autoescape'],
  autoescape=True)
 
class Cards(ndb.Model):
  name = ndb.StringProperty()
  cardnumber = ndb.StringProperty()
  author = ndb.UserProperty()

class Readers(ndb.Model):
  uuid = ndb.StringProperty()
  name = ndb.StringProperty()
  location = ndb.StringProperty()
  readertype = ndb.StringProperty()
  author = ndb.UserProperty()

class AuthResponse(ndb.Model):
  uuid = ndb.StringProperty()
  cardnumber = ndb.StringProperty()
  active = ndb.BooleanProperty(default=True)
  authtime = ndb.IntegerProperty(default=5)
    
class Log(ndb.Model):
  ts = ndb.DateTimeProperty(auto_now_add=True)
  actor = ndb.StringProperty()
  actiontype = ndb.StringProperty()
  cardnumber = ndb.StringProperty()
  data = ndb.StringProperty()
  remote_ip = ndb.StringProperty()
    
def logger_helper(actor='Unknown', actiontype='Unknown', cardnumber='N/A', data='Unknown', remote_ip='0'):
  logging.info('actor: %s, actiontype: %s, data: %s', actor, actiontype, data)
  log=Log(parent=data_key())
  log.actor = actor
  log.actiontype = actiontype
  log.cardnumber = cardnumber
  log.data = data
  log.remote_ip = remote_ip
  log.put()


class AEastern_tzinfo(datetime.tzinfo):
  """Implementation of the Canberra/Melbourne/Sydney timezone."""
  def utcoffset(self, dt):
    return datetime.timedelta(hours=+10) + self.dst(dt)
  def _FirstSunday(self, dt):
    """First Sunday on or after dt."""
    return dt + datetime.timedelta(days=(6-dt.weekday()))
  def dst(self, dt):
    # 2am on the first Sunday in October
    dst_start = self._FirstSunday(datetime.datetime(dt.year, 10, 1, 2))
    # 2 am on the first Sunday in April
    dst_end = self._FirstSunday(datetime.datetime(dt.year, 4, 1, 2))
    if dst_start <= dt.replace(tzinfo=None) < dst_end:
      return datetime.timedelta(hours=1)
    else:
      return datetime.timedelta(hours=0)
  def tzname(self, dt):
    if self.dst(dt) == datetime.timedelta(hours=0):
      return "AEST"
    else:
      return "AEDT"


# Humans  
class MainPage(webapp2.RequestHandler):
  def get(self):

    if users.get_current_user():
      url = users.create_logout_url(self.request.uri)
      url_linktext = 'Logout'
    else:
      url = users.create_login_url(self.request.uri)
      url_linktext = 'Login'

    configs_query = AuthResponse.query(ancestor=data_key()).order(AuthResponse.uuid)
    configs = configs_query.fetch()
    logging.info('Configs: %s', configs)
    cards_query = Cards.query(ancestor=data_key())
    cards = cards_query.fetch()
    cards_array = {}
    for card in cards:
      if card.name:
        cards_array[card.cardnumber] = card.name
    for config in configs:
      if config.cardnumber not in cards_array.keys():
        cards_array[config.cardnumber] = ''
    logging.info('Cards: %s', cards_array)
    
    
    logs_query = Log.query(Log.actiontype.IN(['add_card', 'edit_card', 'delete_card', 'edit_card_name', 'auth']),
                           ancestor=data_key()).order(-Log.ts)
    logs = logs_query.fetch(50)
    for entry in logs:
      entry.ts = datetime.datetime.fromtimestamp(time.mktime(entry.ts.timetuple()), AEastern_tzinfo())
      
    dumps_query = Log.query(Log.actiontype == 'dump', ancestor=data_key()).order(-Log.ts)
    dumps = dumps_query.fetch(50)
    for entry in dumps:
      entry.ts = datetime.datetime.fromtimestamp(time.mktime(entry.ts.timetuple()), AEastern_tzinfo())
      
    template = jinja_environment.get_template('index.html')
    self.response.out.write(template.render(configs=configs,
                                            cards=cards_array,
                                            dumps=dumps,
                                            logs=logs,
                                            url=url,
                                            url_linktext=url_linktext))

        
class AddCard(webapp2.RequestHandler):
  def post(self):
    if users.is_current_user_admin():
      actor = users.get_current_user().email()
      new_entry = AuthResponse(parent=data_key())
      new_entry.uuid = str(self.request.get('uuid'))
      new_entry.cardnumber = str(self.request.get('cardnumber'))
      new_entry.authtime = int(self.request.get('authtime'))
      new_entry.put()
      logger_helper(actor=actor, 
                    actiontype='add_card', 
                    cardnumber=self.request.get('cardnumber'),
                    remote_ip=self.request.remote_addr,
                    data='Adding card #%s to reader %s' % (self.request.get('cardnumber'),
                                                           self.request.get('uuid')))
    self.redirect('/')
    
class EditCard(webapp2.RequestHandler):
  def post(self):
    if users.is_current_user_admin():
      actor = users.get_current_user().email()
      key = ndb.Key(urlsafe=self.request.get('key'))
      entry = key.get()
      entry.uuid = str(self.request.get('uuid'))
      entry.cardnumber = str(self.request.get('cardnumber'))
      entry.authtime = int(self.request.get('authtime'))
      entry.active = bool(self.request.get('active'))
      entry.put()
      logger_helper(actor=actor, 
                    actiontype='edit_card', 
                    cardnumber=self.request.get('cardnumber'),
                    remote_ip=self.request.remote_addr,
                    data='Editing card #%s to reader %s' % (self.request.get('cardnumber'),
                                                             self.request.get('uuid')))
    self.redirect('/')
     
class DeleteCard(webapp2.RequestHandler):
  def post(self):
    if users.is_current_user_admin():
      actor = users.get_current_user().email()
      key = ndb.Key(urlsafe=self.request.get('key'))
      key.delete()
      logger_helper(actor=actor, 
                    actiontype='delete_card', 
                    cardnumber=self.request.get('cardnumber'),
                    remote_ip=self.request.remote_addr,
                    data='Deleting card #%s to reader %s' % (self.request.get('cardnumber'),
                                                             self.request.get('uuid')))
    self.redirect('/')
    
class SetCardName(webapp2.RequestHandler):
  def post(self):
    if users.is_current_user_admin():
      actor = users.get_current_user().email()
      cardnumber = self.request.get('cardnumber')
      cards_query = Cards.query(Cards.cardnumber == cardnumber, ancestor=data_key())
      if len(cards_query.fetch(1)):
        card = cards_query.fetch(1)[0]  
      else:
        card = Cards(parent=data_key())
        card.cardnumber = cardnumber
      card.name = str(self.request.get('name'))
      card.put()
      logger_helper(actor=actor, 
                    actiontype='edit_card_name', 
                    cardnumber=self.request.get('cardnumber'),
                    remote_ip=self.request.remote_addr,
                    data='Set card #%s to name %s' % (self.request.get('cardnumber'),
                                                      self.request.get('name')))
    self.redirect('/')        
# Robots        
class AuthPage(webapp2.RequestHandler):
  def get(self, uuid, cardnumber):
    auth_query = AuthResponse.query(AuthResponse.uuid==uuid,
                                    AuthResponse.cardnumber==cardnumber,
                                    AuthResponse.active==True,
                                    ancestor=data_key())
    auth_responses = auth_query.fetch(2)

    if len(auth_responses)==1:
      api_response=auth_responses[0].authtime
    else:
      api_response='0'
    logger_helper(actor=uuid, 
                  actiontype='auth', 
                  cardnumber=cardnumber,
                  remote_ip=self.request.remote_addr,
                  data='Auth req with card #%s - result: %s, debug: %s' % 
                  (cardnumber, api_response, str(auth_responses)))
    self.response.out.write(json.dumps(api_response))

      
class DumpPage(webapp2.RequestHandler):
  def get(self, uuid):
    dump_query = AuthResponse.query(AuthResponse.uuid==uuid,
                                    ancestor=data_key())
    dump_responses = dump_query.fetch()
    api_response = []
    for dump_response in dump_responses:
      api_response.append({'cardnumber': dump_response.cardnumber, 'authtime': dump_response.authtime})
    logger_helper(actor=uuid, 
                  actiontype='dump', 
                  remote_ip=self.request.remote_addr,
                  data='Dump result: %s' % api_response)
    self.response.out.write(json.dumps(api_response))


application = webapp2.WSGIApplication([
    ('/', MainPage),
    (r'/addcard', AddCard),
    (r'/editcard', EditCard),
    (r'/deletecard', DeleteCard),
    (r'/setcardname', SetCardName),
    (r'/auth/(.*)/(.*)', AuthPage),
    (r'/dump/(.*)', DumpPage),
], debug=True)
